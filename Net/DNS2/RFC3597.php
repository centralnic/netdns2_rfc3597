<?php

/**
* RFC3597 support for Net_DNS2
* @category Networking
* @author Gavin Brown <gavin.brown@centralnic.com>
* @package Net_DNS2_RFC3597
*/
class Net_DNS2_RFC3597 {

	/**
	* setup method, replaces the arrays in Net_DNS2_Lookups with dynamic registries
	*/
	static function setup() {
		Net_DNS2_Lookups::$rr_types_by_id	= new Net_DNS2_RFC3597_DynamicRegistry_RRTypeByID(Net_DNS2_Lookups::$rr_types_by_id);
		Net_DNS2_Lookups::$rr_types_by_name	= new Net_DNS2_RFC3597_DynamicRegistry_RRIDByType(Net_DNS2_Lookups::$rr_types_by_name);
		Net_DNS2_Lookups::$rr_types_class_to_id	= new Net_DNS2_RFC3597_DynamicRegistry_RRIDByClass(Net_DNS2_Lookups::$rr_types_class_to_id);
		Net_DNS2_Lookups::$rr_types_id_to_class = new Net_DNS2_RFC3597_DynamicRegistry_RRClassByID(Net_DNS2_Lookups::$rr_types_id_to_class);
	}

	/**
	* name (and create) a new class, which extends Net_DNS2_RFC3597_RR, for the given unknown RR type
	* @param int $id
	* @return string
	*/
	static function createRRType(int $id) : string {
		$class = sprintf('Net_DNS2_RR_TYPE%u', $id);

		// yes I know eval() is eval
		eval(sprintf('class %s extends Net_DNS2_RFC3597_RR {}', $class));

		return $class;
	}

	/**
	* autoload function
	* @param string $name
	*/
        static public function autoload($name) {
            if (__CLASS__ == substr($name, 0, strlen(__CLASS__))) @include_once(str_replace('_', '/', $name).'.php');
        }
}
