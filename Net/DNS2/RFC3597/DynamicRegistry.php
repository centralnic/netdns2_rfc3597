<?php

/**
* base class for dynamic registry objects
* @category Networking
* @author Gavin Brown <gavin.brown@centralnic.com>
* @package Net_DNS2_RFC3597
*/
abstract class Net_DNS2_RFC3597_DynamicRegistry extends ArrayIterator {

	/**
	* @see ArrayIterator::offsetGet()
	* @param mixed $index
	* @return mixed
	*/
	function offsetGet($index) {
		if (parent::offsetExists($index)) {
			return parent::offsetGet($index);

		} else {
			$value = $this->createValueFor($index);
			$this->offsetSet($index, $value);
			return $value;

		}
	}

	/**
	* given an index (for a non-existent entry in the registry), compute what the value *should* be
	* @param mixed $index
	* @return mixed
	*/
	protected abstract function createValueFor($index);
}
