<?php

/**
* base class for RFC4597 RR objects
* @category Networking
* @author Gavin Brown <gavin.brown@centralnic.com>
* @package Net_DNS2_RFC3597
*/
class Net_DNS2_RFC3597_RR extends Net_DNS2_RR {

	/**
	* the binary data for this RR - by definition we don't know what might be in this data, so applications will need to parse it themselves
	*/
	public $data;

	/**
	* method to return a RR as a string; not to 
	* be confused with the __toString() magic method.
	* @return string
	*/
	protected function rrToString() : string {
		return sprintf("\# %u %s", strlen($this->data), bin2hex($this->data));
	}

	/**
	* parses a RR from a standard DNS config line
	* @param array $rdata a string split line of values for the rdata
	* @return bool
	*/
	protected function rrFromString(array $rdata) : bool {
		// first element must be a literal a backslash immediately followed by a hash sign
		if ('\#' != array_shift($rdata)) return false;

		// discard the next value which is an unsigned decimal integer specifying the RDATA length in octets
		array_shift($rdata);

		// discard anything not hex
		$data = preg_replace('/[^0-9a-f]/', '', strtolower(implode('', $rdata)));

		// must be an even number of hex digits
		if (0 != strlen($data) % 2) return false;

		// convert the hex to binary
		$this->data = hex2bin($data);

		return true;
	}

	/**
	* sets a Net_DNS2_RR from a Net_DNS2_Packet object
	* @param Net_DNS2_Packet &$packet a Net_DNS2_Packet packet to parse the RR from
	* @return bool
	*/
	protected function rrSet(Net_DNS2_Packet &$packet) : bool {
		// just copy the binary data
		$this->data = $this->rdata;
		return true;
	}

	/**
	* returns a binary packet DNS RR object
	* @param Net_DNS2_Packet &$packet a Net_DNS2_Packet packet use for compressed names
	* @return mixed either returns a binary packed string or  null on failure
	*/
	protected function rrGet(Net_DNS2_Packet &$packet) {
		$packet->offset += strlen($this->data);
		return $this->data;
	}
}
