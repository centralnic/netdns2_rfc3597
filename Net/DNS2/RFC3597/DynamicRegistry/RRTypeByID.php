<?php

/**
* dynamic registry for RR types indexed by numeric ID
* @category Networking
* @author Gavin Brown <gavin.brown@centralnic.com>
* @package Net_DNS2_RFC3597
*/
class Net_DNS2_RFC3597_DynamicRegistry_RRTypeByID extends Net_DNS2_RFC3597_DynamicRegistry {

	/**
	* @see Net_DNS2_RFC3597_DynamicRegistry::offsetGet()
	* @param mixed $index
	* @return mixed
	*/
	function offsetExists($index) {
		if (parent::offsetExists($index)) {
			return true;

		} else {
			return ($index >= 0 && $index <= 65535);

		}
	}

	/**
	* given an index (for a non-existent entry in the registry), compute what the value *should* be
	* @param mixed $index
	* @return mixed
	*/
	protected function createValueFor($index) {
		return sprintf('TYPE%u', $index);
	}
}
