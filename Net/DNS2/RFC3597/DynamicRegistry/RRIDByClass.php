<?php

/**
* dynamic registry for RR IDs indexed by class
* @category Networking
* @author Gavin Brown <gavin.brown@centralnic.com>
* @package Net_DNS2_RFC3597
*/
class Net_DNS2_RFC3597_DynamicRegistry_RRIDByClass extends Net_DNS2_RFC3597_DynamicRegistry {

	/**
	* @see Net_DNS2_RFC3597_DynamicRegistry::offsetGet()
	* @param mixed $index
	* @return mixed
	*/
	function offsetExists($index) {
		if (parent::offsetExists($index)) {
			return true;

		} else {
			return (1 == preg_match('/^Net_DNS2_RR_TYPE(\d+)$/', $index));

		}
	}

	/**
	* given an index (for a non-existent entry in the registry), compute what the value *should* be
	* @param mixed index
	* @return mixed
	*/
	protected function createValueFor($index) {
		if (1 == preg_match('/^Net_DNS2_RR_TYPE(\d+)$/', $index, $matches)) {
			return intval($matches[1]);

		} else {
			return NULL;

		}
	}
}
