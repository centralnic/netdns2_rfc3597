<?php

/**
* @category Networking
* @author Gavin Brown <gavin.brown@centralnic.com>
* @package Net_DNS2_RFC3597
*/
class Net_DNS2_RFC3597_Exception extends Net_DNS2_Exception {
}
