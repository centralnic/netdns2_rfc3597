# `Net_DNS2_RFC3597` - RFC3597 support for `Net_DNS2`

This package adds support for unsupported RR types (as described in [RFC3597](https://tools.ietf.org/html/rfc3597)) to [`Net_DNS2`](https://netdns2.com).

Normally, `Net_DNS2` will throw an exception if it parses a response packet and sees a record that it doesn't have a corresponding `Net_DNS2_RR_*` class for. This can cause problems with things like zone file parsing and zone transfers.

`Net_DNS2_RFC3597` resolves this problem by replacing the RR type registries in `Net_DNS2_Lookups` with objects (based on `ArrayIterator`), which will dynamically define a new class for each unsupported RR type that `Net_DNS2` sees.

`Net_DNS2_RFC3597` also allows you to query for unsupported RR types.

## Installation

```
composer require centralnic/net_dns2_rfc3597
```

## Requirements

* `Net_DNS2` which may be installed using a vendor pacakge, or using `pear` or `composer`

## Using `Net_DNS2_RFC3597`

```php
<?php

require 'vendor/autoload.php';

Net_DNS2_RFC3597::setup();

$resolver = new Net_DNS2_Resolver;

$resolver->query('example.com.', 'TYPE1234');

```

## Copyright

Copyright (c) 2019 CentralNic Group plc. All rights reserved.
